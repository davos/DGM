from Helpers import *
from data_loader import DataLoader2
import torch.nn as nn
from glob import glob
from collections import defaultdict
import json

device = torch.device('cuda') if torch.cuda.is_available() else torch.device('cpu')

data_type = "results/credit/"
methods = ['mean']
mse = nn.MSELoss()
batch_size = 16

results = {}

test_data, _ = pd.read_pickle(data_type + "test_data.pickle")
test_loader = DataLoader(test_data, batch_size=batch_size, shuffle=False)

df = pd.read_excel("data/defaultofcreditcardclients.xls", header=0)
y = torch.as_tensor(np.asarray(df.iloc[1:, -1], dtype=int))
y = y.to(int)
x = torch.as_tensor(np.asarray(df.iloc[1:, 1:-1], dtype=float)).to(device)
x = x.to(float)
NORMALISING_STRATEGY = 'minmax'
data_loader = DataLoader2((x, y), dataset="credit", device=device, normalising_strategy=NORMALISING_STRATEGY)
test_data_og = data_loader.test_x
test_loader_og = DataLoader(test_data_og, batch_size=batch_size, shuffle=False)

model_paths = glob(data_type + '*.model')
models = [torch.load(path) for path in model_paths]
results = defaultdict(lambda: [])

for method in methods:
    predicted_values, target_values = torch.Tensor([]).to(device), torch.Tensor([]).to(device)
    for model in models:
        iterator = tqdm(zip(test_loader, test_loader_og), total=len(test_data_og)//batch_size)
        for masked_batch, og_batch in iterator:
            nan_rows = torch.isnan(masked_batch).any(axis=1)
            masked_batch_only_nan_rows = masked_batch[nan_rows]
            nan_mask = torch.isnan(masked_batch_only_nan_rows)
            test_data_norm = model.normalise(masked_batch_only_nan_rows.clone())
            if any(model.dequantization):
                test_data_norm[:, model.dequantization] = test_data_norm[:, model.dequantization] + (
                            1 - torch.rand(test_data_norm[:, model.dequantization].shape)).to(model.device) / (
                                                             model.scaling_params[0][model.dequantization] -
                                                             model.scaling_params[1][model.dequantization])
            out = inference(test_data_norm.clone().detach(), model, n=1000, method=method)

            out = model.inv_normalise(out)
            out[:, model.dequantization] = torch.ceil(out[:, model.dequantization])
            for key, pred, target in zip(torch.where(nan_mask)[1], out[nan_mask], og_batch[nan_rows][nan_mask]):
                results[key.item()+1] += [(pred.item(), target.item())]

    with open(data_type + 'results_mean.json', 'w') as fp:
        json.dump(results, fp)

    preds, targets = [], []
    for val in results.values():
        for pred, target in val:
            preds += [pred]
            targets += [target]

    RMSE = torch.sqrt(mse(torch.tensor(preds), torch.tensor(targets))).item()

    print(f'\nMethod: {method}, RMSE: {RMSE:.3f}')
