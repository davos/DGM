import torch
import torch.nn as nn
import numpy as np


class RealNVP(nn.Module):
    def __init__(self, nets, nett, q_dist, mask, prior, scaling_params, normalising_strategy, D=2, L=1, device='cpu', dequantization=False):
        super(RealNVP, self).__init__()

        print('RealNVP by KDKDK. Skrrrt, brrrrrrr')

        self.dequantization = dequantization
        self.mask = nn.Parameter(mask, requires_grad=False)
        self.prior = prior
        self.num_flows = len(mask)
        self.t = torch.nn.ModuleList([nett(i) for i in range(self.num_flows)]).to(float).to(device)
        self.s = torch.nn.ModuleList([nets(i) for i in range(self.num_flows)]).to(float).to(device)

        self.device = device
        self.q_distribution = q_dist
        self.D = D
        self.L = L
        self.scaling_params = scaling_params
        self.normalising_strategy = normalising_strategy

    def coupling(self, x, index, forward=True):
        # x: input, either images (for the first transformation) or outputs from the previous transformation
        # index: it determines the index of the transformation
        # forward: whether it is a pass from x to y (forward=True), or from y to x (forward=False)

        # (xa, xb) = torch.chunk(x, 2, 1)
        if not index % 2:
            inv_mask = self.mask[index + 1]
        else:
            inv_mask = self.mask[index - 1]

        xa = x[:, self.mask[index]]
        xb = x[:, inv_mask]

        s = self.s[index](xa)
        t = self.t[index](xa)

        if forward:
            # yb = f^{-1}(x)
            # yb = (xb - t) * torch.exp(-s)
            x[:, inv_mask] = (xb - t) * torch.exp(-s)

        else:
            # xb = f(y)
            # yb = torch.exp(s) * xb + t
            x[:, inv_mask] = torch.exp(s) * xb + t
        return x, s

    def permute(self, x):
        return x.flip(1)

    def f(self, x):
        log_det_J, z = x.new_zeros(x.shape[0]), x
        for i in range(self.num_flows):
            z, s = self.coupling(z, i, forward=True)
            # z = self.permute(z)
            log_det_J = log_det_J - s.sum(dim=1)

        return z, log_det_J

    def f_inv(self, z):
        x = z
        for i in reversed(range(self.num_flows)):
            # x = self.permute(x)
            x, _ = self.coupling(x, i, forward=False)
        return x

    def forward(self, x, reduction='avg'):

        if torch.any(x.isnan()):
            # Get L tiles
            x = x.tile((self.L, 1))
            # Calculate isnan mask
            mask_missing = x.isnan()

            # Initialise p(x)
            px = 0

            for col in range(x.shape[1]):
                # Get mask for col
                col_mask = mask_missing[:, col]
                # Get number of missing values in current col
                num_missing = sum(col_mask)
                if not num_missing:
                    continue
                # Sample missing points for the col'th dimension and replace the sampled values into x
                samples_missing = self.q_distribution[col].sample((num_missing.item(),))
                x[:, col][col_mask] = samples_missing
                # Calculate log_prob
                log_qs = self.q_distribution[col].log_prob(samples_missing)
                px -= (1 / self.L) * log_qs.sum()

            z, log_det_J = self.f(x)
            if reduction == 'sum':
                px += (1 / self.L) * (self.prior.log_prob(z) + log_det_J).sum()
            else:
                px += (1 / self.L) * (self.prior.log_prob(z) + log_det_J).mean()
        else:
            z, log_det_J = self.f(x)
            if reduction == 'sum':
                px = (self.prior.log_prob(z) + log_det_J).sum()
            elif reduction == 'avg':
                px = (self.prior.log_prob(z) + log_det_J).mean()
            else:
                px = -(self.prior.log_prob(z) + log_det_J)

        return -px

    def sample(self, batchSize):
        z = self.prior.sample((batchSize, self.D))
        z = z[:, 0, :]
        x = self.f_inv(z)
        return x.view(-1, self.D)

    def inv_normalise(self, x):
        if self.normalising_strategy == 'minmax':
            return x * (self.scaling_params[1] - self.scaling_params[0]) + self.scaling_params[0]
        else:
            return x * self.scaling_params[1] + self.scaling_params[0]

    def normalise(self, x):
        if self.normalising_strategy == 'minmax':
            return (x - self.scaling_params[0]) / (self.scaling_params[1] - self.scaling_params[0])
        else:
            return (x - self.scaling_params[0]) / self.scaling_params[1]
