import random
import sys
import os
import pandas as pd
import torch
import csv
import numpy as np
import matplotlib.pyplot as plt
from sklearn.datasets import make_moons
import torch.distributions as dists
from tqdm import tqdm
from torch.utils.data import DataLoader
import pickle


def evaluation(model, test_loader, name=None, model_best=None, epoch=None):
    # EVALUATION
    if model_best is None:
        # load best performing model
        model_best = torch.load(name + '.model')

    model_best.eval()
    loss = 0.
    N = 0.
    for indx_batch, test_batch in enumerate(test_loader):
        if hasattr(model, 'dequantization'):
            if any(model.dequantization):
                test_batch[:, model.dequantization] = test_batch[:, model.dequantization] + (1 - torch.rand(test_batch[:, model.dequantization].shape)).to(model.device)/(model.scaling_params[0][model.dequantization]-model.scaling_params[1][model.dequantization])

        loss_t = model_best.forward(test_batch, reduction='sum')
        loss = loss + loss_t.item()
        N = N + test_batch.shape[0]
    loss = loss / N

    # if epoch is None:
    #     print(f'FINAL LOSS: nll={loss}')
    # else:
    #     print(f'Epoch: {epoch}, val nll={loss}')

    return loss


def create_mask(shape, ratio=.5):
    zeros = int(shape * ratio)
    ones = shape - zeros
    lst = []
    for i in range(shape):
        if zeros > 0 and ones > 0:
            if np.random.uniform() > ratio:
                lst.append(0)
                zeros -= 1
            else:
                lst.append(1)
                ones -= 1
        elif zeros > 0:
            lst.append(0)
            zeros -= 1
        else:
            lst.append(1)
            ones -= 1
    return np.asarray(lst, dtype=bool)


def generate_masks(num_layers, data_shape, ratio=.5):
    mask = []
    for idx in range(num_layers):
        msk = create_mask(data_shape, ratio)
        mask.append(np.invert(msk))
        mask.append(msk)

    mask = np.asarray(mask)
    return torch.tensor(mask)


def plot_curve(nll_val, result_dir, name):
    plt.plot(np.arange(len(nll_val)), nll_val, linewidth='3')
    plt.xlabel('epochs')
    plt.ylabel('nll')
    plt.savefig(result_dir + name + '_nll_val_curve.pdf', bbox_inches='tight')
    plt.close()


def plot_latent(
        model,
        data=None,
        mask=None,
        result_dir='results/',
        name=None,
        epoch=None,
        save=False,
        cross_fold='0',
        colorpal="tab10",
        alphaVal = 0.8
):
    if data is None:
        x, y = make_moons(1000, noise=0.1)
        x = torch.tensor(x.astype(np.float32))
        mask = None
    else:
        (x, y) = data
    f, (ax1, ax2) = plt.subplots(1, 2, figsize=(10, 5), dpi=400)
    if mask is None:
        ax1.scatter(x[:, 0], x[:, 1], c=y, cmap=colorpal, alpha=alphaVal)
    else:
        x = x.cpu()
        filter1 = (mask == 0).cpu()
        ax1.scatter(x[:, 0][filter1], x[:, 1][filter1], c=y[filter1], cmap=colorpal, s=64, marker='.', alpha=alphaVal)
        filter2 = (mask > 0).cpu()
        ax1.scatter(x[:, 0][filter2], x[:, 1][filter2], c=y[filter2], cmap=colorpal, s=32, marker='^', linewidth=1,
                    edgecolors='black', alpha=alphaVal)
    ax1.set_title('Data (normalised)')
    x = x.to(model.device)
    z, _ = model.f(x)
    z = z.detach().cpu()
    min_lim = z.min().item()
    max_lim = z.max().item()
    if mask is None:
        ax2.scatter(z[:, 0], z[:, 1], c=y, cmap=colorpal, alpha=alphaVal)
    else:
        ax2.scatter(z[:, 0][filter1], z[:, 1][filter1], c=y[filter1], cmap=colorpal, s=64, marker='.', alpha=alphaVal)
        ax2.scatter(z[:, 0][filter2], z[:, 1][filter2], c=y[filter2], cmap=colorpal, s=32, marker='^', linewidth=1,
                    edgecolors='black', alpha=alphaVal)
    ax2.set_title('Transformed space')
    margin = 0.1
    ax2.set_xlim(min_lim - margin, max_lim + margin)
    ax2.set_ylim(min_lim - margin, max_lim + margin)
    ax1.grid(color='gray', linestyle='--', linewidth=0.8, alpha=0.5)
    ax2.grid(color='gray', linestyle='--', linewidth=0.8, alpha=0.5)

    f.tight_layout()
    if save:
        plt.savefig(result_dir + 'latent/' + name + f'_cv{cross_fold}_epoch{epoch}.pdf')
        plt.close()
    else:
        plt.show()


def plot_data(x, y, mask, epoch=0, save=False):
    n_classes = max(y) + 1
    # for i, row in enumerate(mask.t()):
    #     y[row] = n_classes+i+1
    if mask is not None:
        mask = mask.sum(1).to(bool)
        # y = y.astype(float)
        y[mask.cpu()] += n_classes
    colors = ['blue', 'red', 'green', 'orange']
    yc = [colors[i] for i in y]
    plt.figure()
    plt.title("Data")
    plt.scatter(x.cpu()[:, 0], x.cpu()[:, 1], c=yc)
    plt.tight_layout()
    if save:
        plt.savefig(f'data/data{epoch}.pdf')
        plt.close()
    else:
        plt.show()


def training(
        result_dir,
        name,
        max_patience,
        num_epochs,
        model,
        optimizer,
        train_data,
        train_labels,
        val_data,
        batch_size,
        cross_fold,
        constant_sample=None
):

    # For plotting
    missing_mask = torch.sum(train_data.isnan(), 1)

    if constant_sample is None:
        training_loader = DataLoader(train_data, batch_size=batch_size, shuffle=True)
        val_loader = DataLoader(val_data, batch_size=batch_size, shuffle=False)
    else:
        # If samples are kept for multiple epochs
        # Generate L copies of data by extending each row (RAMFINITY)
        train_data = train_data.tile((1, model.L))
        # Permute data to shuffle data but keep all L samples of the same point together
        shuffling_perm = torch.randperm(len(train_data))
        train_data = train_data[shuffling_perm]
        train_labels = train_labels[shuffling_perm]
        missing_mask = missing_mask[shuffling_perm]
        # Reshape so each of the L copies is a row
        train_data = train_data.reshape((-1, val_data.shape[1]))
        batch_size *= model.L
        # val_data does not need to be shuffled!
        val_data = val_data.tile((1, model.L)).reshape((-1, train_data.shape[1]))

    train_data = train_data.to(model.device)
    val_data = val_data.to(model.device)

    nll_val = []
    best_nll = 1000.
    patience = 0
    log_q = 0

    iterator = tqdm(range(num_epochs))
    # Main loop
    for e in iterator:
        if constant_sample is not None and not e % constant_sample:
            # Now we sample
            train_data_, log_q = sample_missing_values(train_data.clone().detach(), model)
            training_loader = DataLoader(train_data_, batch_size=batch_size, shuffle=False)
            val_data_, _ = sample_missing_values(val_data.clone().detach(), model)
            val_loader = DataLoader(val_data_, batch_size=batch_size, shuffle=False)
            # if train_labels is not None:
            #     plot_data(train_data_[::model.L], train_labels.copy(), train_data.isnan()[::model.L], e)

        # TRAINING
        model.train()
        for indx_batch, batch in enumerate(training_loader):
            if hasattr(model, 'dequantization'):
                if any(model.dequantization):
                    batch[:, model.dequantization] = batch[:, model.dequantization] + (1 - torch.rand(batch[:, model.dequantization].shape)).to(model.device)/(model.scaling_params[0][model.dequantization]-model.scaling_params[1][model.dequantization])
            loss = model.forward(batch) - log_q

            optimizer.zero_grad()
            loss.backward(retain_graph=True)
            optimizer.step()
        # Validation

        loss_val = evaluation(model, val_loader, model_best=model, epoch=e)
        nll_val.append(loss_val)  # save for plotting
        if e == 0:
            # print('saved!')
            torch.save(model, result_dir + name + str(cross_fold) + '.model')
            best_nll = loss_val
            model_saved = e
        else:
            if loss_val < best_nll:
                # print('saved!')
                torch.save(model, result_dir + name + str(cross_fold) + '.model')
                best_nll = loss_val
                patience = 0
                model_saved = e
                if constant_sample is not None and result_dir[-6:] == 'moons/' or result_dir[-8:] == 'circles/' or result_dir[-6:] == 'blobs/':
                    if not (os.path.exists(result_dir + 'latent/')):
                        os.mkdir(result_dir + 'latent/')
                    # plot_train_data = train_data_[::model.L].clone().detach()
                    # plot_train_data[:, model.dequantization] = plot_train_data[:, model.dequantization] + (
                    #             1.0 - torch.rand(plot_train_data[:, model.dequantization].shape)).to(model.device)/(train_max[model.dequantization]-train_min[model.dequantization])
                    # plot_latent(data=(plot_train_data, train_labels), model=model,
                    #             mask=missing_mask, result_dir=result_dir, name=name, epoch=e, save=True, cross_fold=cross_fold)
            else:
                patience = patience + 1

        with open(result_dir + name + str(cross_fold) + '_nll.pickle', 'wb') as f:
            pickle.dump(np.asarray(nll_val), f)

        iterator.set_postfix(epoch=e, loss=loss_val, last_saved=model_saved, best_loss=best_nll)
        if patience > max_patience:
            break

    nll_val = np.asarray(nll_val)

    return nll_val


# Helper functions
def get_moons(n_pts=10000):
    """Get standard nonlinearly separable two-moons 2D dataset"""
    x0, y0 = make_moons(n_pts, noise=.1)
    x0 = torch.as_tensor(x0).float()

    mu = x0.mean(0, keepdims=True)
    sig = x0.std(0, keepdims=True)
    x0 = (x0 - mu) / sig

    return x0, y0, mu, sig


def remove_data(x, p):
    shapes = x.shape
    mask = torch.FloatTensor(*shapes).uniform_() < p
    x[mask] = np.nan
    return x

def remove_data2(x, p):
    mask = torch.FloatTensor(x.shape[0]).uniform_() < p
    temp = x[mask]
    for i in range(len(temp)):
        if torch.rand(1) >= 0.5:
            temp[i][0] = np.nan
        else:
            temp[i][1] = np.nan
    x[mask] = temp
    return x

def create_q_dists(x, normalising_strategy="minmax", device='cpu'):

    q_distributions = []
    for i in range(x.shape[1]):
        if normalising_strategy == "minmax":
            q_distributions += [dists.Normal(loc=torch.tensor([0.5]).to(float).to(device), scale=torch.tensor([0.5]).to(float).to(device))]
        else:
            q_distributions += [dists.Normal(loc=torch.tensor([0.0]).to(float).to(device), scale=torch.tensor([1.0]).to(float).to(device))]
    return q_distributions


def sample_missing_values(x, model, reduction='sum'):
    # Calculate isnan mask
    mask_missing = x.isnan()
    # Initialise log_q
    log_q = torch.zeros(len(x)).to(model.device)

    for col in range(x.shape[1]):
        # Get mask for col
        col_mask = mask_missing[:, col]
        # Get number of missing values in current col
        num_missing = sum(col_mask)
        if not num_missing:
            continue
        # Sample missing points for the col'th dimension and replace the sampled values into x
        samples_missing = model.q_distribution[col].sample((num_missing.item(),)).flatten()
        x[:, col][col_mask] = samples_missing
        # Calculate log_prob
        log_qs = model.q_distribution[col].log_prob(samples_missing)
        log_q[col_mask] -= (1 / model.L) * log_qs

    if reduction == 'sum':
        return x, log_q.sum()
    else:
        return x, log_q


def estimate_missing_values(x, model, n=1000):
    if not torch.any(x.isnan()):
        return
    if len(x.shape) > 1:
        samples = x.tile((1, n)).reshape((n*x.shape[0], x.shape[-1]))
    else:
        samples = x.tile((1, n)).reshape((n, x.shape[-1]))
    samples, log_q = sample_missing_values(samples.clone().detach(), model, reduction='none')

    px = torch.exp(model.forward(samples.clone().detach(), reduction='none') - log_q)

    return samples.detach(), px.detach()


def inference(x, model, n=1000, method='mean'):
    samples, p_samples = estimate_missing_values(x, model, n)
    samples = samples.reshape((x.shape[0], n, x.shape[-1]))
    p_samples = p_samples.reshape((x.shape[0], n))

    if method == 'mean':
        return torch.sum(samples * p_samples[:, :, None], axis=1) / p_samples.sum(1)[:, None]
    elif method == 'mode':
        nan_mask = torch.isnan(x)
        for i, row in enumerate(nan_mask):
            for j, col in enumerate(row):
                if col:
                    values = samples[i, :, j]
                    weights = p_samples[i]
                    hist = torch.histogram(values.cpu(), bins=100, weight=weights.cpu())
                    argmax = torch.argmax(hist[0])
                    lower = hist[1][argmax].item()
                    upper = hist[1][argmax + 1].item()
                    sample = np.random.choice(values[torch.logical_and(lower < values, values < upper)].cpu())
                    x[i, j] = sample
        return x
    else:
        raise ValueError("Method not found")


class CustomCategorical:
    def __init__(self, min, max, device):
        self.min = int(min.item())
        self.max = int(max.item()) + 1
        self.device = device

    def sample(self, sample_shape=torch.Size()):
        samples = (torch.randint(low=self.min, high=self.max, size=sample_shape) - self.min)/(self.max - self.min - 1)
        return samples.to(float).to(self.device)

    def log_prob(self, value):
        log_ps = torch.ones(value.shape[0]) * torch.log(torch.tensor([1. / (self.max - self.min)]))
        return log_ps.to(float).to(self.device)
