import torch
import torch.nn as nn
import numpy as np
import os
import pandas as pd
import pickle
from sklearn.datasets import make_moons, make_blobs, make_circles
from sklearn.model_selection import train_test_split
from torch.utils.data import Dataset, DataLoader
from RealNVP import RealNVP
from Helpers import *
from data_loader import DataLoader2
import random

random.seed(1)
np.random.seed(1)
torch.manual_seed(1)

result_dir = 'results/'
if not (os.path.exists(result_dir)):
    os.mkdir(result_dir)
name = 'realnvp'

device = torch.device('cuda') if torch.cuda.is_available() else torch.device('cpu')

# Hyperparameters
M = 256  # the number of neurons in scale (s) and translation (t) nets

lr = 1e-3  # learning rate
drp_percent = 0.1 # value between 0 and 1
num_epochs = 300  # max. number of epochs
max_patience = 300  # an early stopping is used, if training doesn't improve for longer than 20 epochs, it is stopped
batch_size = 16
# The number of invertible transformations
num_flows = 6  # MUST be an even integer
dataset = "blobs"

if dataset == "credit":
    ## Credit card dataset
    df = pd.read_excel("data/defaultofcreditcardclients.xls", header=0)
    y = torch.as_tensor(np.asarray(df.iloc[1:, -1], dtype=int))
    y = y.to(int)
    x = torch.as_tensor(np.asarray(df.iloc[1:, 1:-1], dtype=float)).to(device)
    x = x.to(float)
    NORMALISING_STRATEGY = 'minmax'
    data_loader = DataLoader2((x, y), dataset=dataset, device=device, normalising_strategy=NORMALISING_STRATEGY)
elif dataset == "moons":
    x, y = make_moons(10000, noise=0.1, random_state=7)
elif dataset == "circles":
    x, y = make_circles(n_samples=1000, noise=0.1, factor=0.2)
elif dataset == "blobs":
    x, y = make_blobs(n_samples=10000, centers=6, n_features=2, random_state=7)

x = torch.tensor(x).to(device)
y = torch.tensor(y)
NORMALISING_STRATEGY = 'mustd'
data_loader = DataLoader2((x, y), dataset=dataset, drp_percent=drp_percent, device=device, normalising_strategy=NORMALISING_STRATEGY)

result_dir = result_dir + dataset + f"/p{drp_percent}"+'/'
if not (os.path.exists(result_dir)):
    os.mkdir(result_dir)

# TODO: distinguish between removing all cat vs. not.
D = x.shape[-1]  # input dimension

# scale (s) network
nets = lambda i: nn.Sequential(
    nn.Linear(torch.div((D + i % 2), 2, rounding_mode='floor').item(), M), nn.LeakyReLU(),
    nn.Linear(M, M), nn.LeakyReLU(),
    nn.Linear(M, M), nn.LeakyReLU(),
    nn.Linear(M, M), nn.LeakyReLU(),
    nn.Linear(M, torch.div((D + (i + 1) % 2), 2, rounding_mode='floor').item()), nn.Tanh()
)

# translation (t) network
nett = lambda i: nn.Sequential(
    nn.Linear(torch.div((D + i % 2), 2, rounding_mode='floor').item(), M), nn.LeakyReLU(),
    nn.Linear(M, M), nn.LeakyReLU(),
    nn.Linear(M, M), nn.LeakyReLU(),
    nn.Linear(M, M), nn.LeakyReLU(),
    nn.Linear(M, torch.div((D + (i + 1) % 2), 2, rounding_mode='floor').item()), nn.Tanh()
)

mask = generate_masks(num_flows // 2, D)

# Prior (a.k.a. the base distribution): Gaussian
prior = torch.distributions.MultivariateNormal(torch.zeros(D).to(float).to(device), torch.eye(D).to(float).to(device))

# Log device type.
with open("train.txt", "w") as fp:
    fp.write(f"DEVICE: {device}")

with open(result_dir + 'test_data.pickle', 'wb') as fp:
    pickle.dump((data_loader.test_x, data_loader.test_y), fp)

for i in range(5):  # 5 fold cross-val-loop
    train_x, train_scaling_param_0, train_scaling_param_1, train_y, val_x, val_y = data_loader.create_k_fold(seed=i)
    # check if any of the data has been removed i.e. if we remove any categorical features

    # Init RealNVP
    q_dists = create_q_dists(train_x, normalising_strategy=NORMALISING_STRATEGY, device=device)

    model = RealNVP(
        nets,
        nett,
        q_dists,
        mask,
        prior,
        scaling_params=(train_scaling_param_0, train_scaling_param_1),
        normalising_strategy=NORMALISING_STRATEGY,
        D=D,
        L=1,
        device=device,
        dequantization=data_loader.dequantization_type
    )

    # OPTIMIZER
    optimizer = torch.optim.Adamax([p for p in model.parameters() if p.requires_grad == True], lr=lr)

    # Training procedure
    nll_val = training(
        result_dir=result_dir,
        name=name,
        max_patience=max_patience,
        num_epochs=num_epochs,
        model=model,
        optimizer=optimizer,
        train_data=train_x,
        train_labels=train_y,
        val_data=val_x,
        batch_size=batch_size,
        cross_fold=i,
        constant_sample=10
    )

    plot_curve(nll_val, result_dir, name + str(i))
