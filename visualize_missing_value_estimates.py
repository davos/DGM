from Helpers import *
import seaborn as sns
from sklearn.datasets import make_moons, make_blobs, make_circles
import matplotlib.patches as mpatches
import matplotlib.pyplot as plt

device = torch.device('cuda') if torch.cuda.is_available() else torch.device('cpu')

# VARIABLES
alphaVal = 0.7
linewidth = 0.1
binnr = 30
#font
titlesize = 18

# plot 1
dataset1 = "moons"
drp_rate1 = "0.7"
model_nr1 = "3"
# DEFINE MISSING DATA POINT
x1 = torch.tensor([0.5, np.nan]).to(float).to(device)
plot_title1 = fr"{dataset1} (p{drp_rate1}), {r'$x_{mis}=$'}({x1[0]}, {x1[1]})"
model_path1 = f'complete_data/{dataset1}/p{drp_rate1}/realnvp{model_nr1}.model'

# plot 2
dataset2 = "blobs"
drp_rate2 = drp_rate1
model_nr2 = "3"
# DEFINE MISSING DATA POINT
x2 = torch.tensor([np.nan, 5]).to(float).to(device)
plot_title2 = f"{dataset2} (p{drp_rate2}), {r'$x_{mis}=$'}({x2[0]}, {x2[1]})"
model_path2 = f'complete_data/{dataset2}/p{drp_rate2}/realnvp{model_nr2}.model'

# COLORS AND PLOT PARAMETERS
colorpal = "tab20b"
colors = {"Rød":      "#F44336",
          "Grøn":     "#2ECC71",
          "Lilla":    "#AF7AC5",
          "Blå":      "#3498DB",
          "Orange":   "#FB8C00",
          "Lys teal": "#76D7C4",
          "Gul":      "#F7DC6F",
          "Blå/grå":  "#5D6D7E"
}
true_color = "goldenrod" # list(colors.values())[-2]
sample_color = "royalblue" #list(colors.values())[-1]

#region Model1 plot
if dataset1 == "moons":
    histplotscale = 3
elif dataset1 == "blobs":
    histplotscale = 0.05

# LOAD MODEL
model = torch.load(model_path1)

# CREATE DATA
if dataset1 == "moons":
    data, y = make_moons(10000, noise=0.1, random_state=7)
    true_threshold = 0.15
elif dataset1 == "blobs":
    data, y = make_blobs(n_samples=10000, centers=6, n_features=2, random_state=7)
    true_threshold = 0.5

data = torch.tensor(data)
yc = [list(colors.values())[i] for i in y]

# NORMALISE DATA FOR MODELE ESTIMATION
x1 = model.normalise(x1)
samples, px = estimate_missing_values(x1, model, n=10000)
# INVERSE NORMALISATION TO TRANFORM INTO ORIGINAL DOMAIN
x1 = model.inv_normalise(x1).cpu()
samples = model.inv_normalise(samples).detach().cpu()
px = px.cpu()

# DEFINE MASK THAT TAKES ALL DATA AROUND THE KNOWN DATA-POINT WITH THRESHOLD "true_threshold"
if torch.isnan(x1[0]):
    true_mask = torch.logical_and(data[:, 1] > x1[1] - true_threshold, data[:, 1] < x1[1] + true_threshold)
    true_dist = data[true_mask]
else:
    true_mask = torch.logical_and(data[:, 0] > x1[0] - true_threshold, data[:, 0] < x1[0] + true_threshold)
    true_dist = data[true_mask]

# DEFINE PLOT LIMITS
ylim = (min(data.cpu()[:, 1])-0.2, max(data.cpu()[:, 1])+0.2)
xlim = (min(data.cpu()[:, 0])-0.2, max(data.cpu()[:, 0])+0.2)

# PLOTTING
fig = plt.figure(figsize=(20,5))
ax = fig.add_subplot(141, label="1")
ax2 = fig.add_subplot(141, label="2", frame_on=False)
ax3 = fig.add_subplot(141, label="3", frame_on=False)

ax2.set_xticks([])
ax2.set_yticks([])
ax3.set_xticks([])
ax3.set_yticks([])

ax.scatter(data.cpu()[:, 0], data.cpu()[:, 1], c=yc, alpha=alphaVal) # cmap=colorpal,
ax.set_title(plot_title1, fontsize=titlesize)
ax.grid(color='gray', linestyle='--', linewidth=0.8, alpha=0.5)
ax.set_xlabel(r"$x_1$", fontsize=16)
ax.set_ylabel(r"$x_2$", fontsize=16)
ax.tick_params(axis='both', which="major", labelsize=12)
ax.set_ylim(*ylim)
ax.set_xlim(*xlim)

if x1[0].isnan():
    ax.axhline(x1[1].item())
    values = samples[:, 0]

    ax2 = sns.histplot(x=true_dist[:, 0], bins=binnr, stat='density', alpha=alphaVal, kde=False, color=true_color,
                       linewidth=linewidth, line_kws=dict(color='goldenrod', alpha=0.5, linewidth=1.5, label='KDE of WPoPS'))

    ax3 = sns.histplot(x=values, weights=px, bins=binnr*2, stat='density', alpha=alphaVal-0.2, kde=False, color=sample_color,
                       linewidth=linewidth, line_kws=dict(color='royalblue', alpha=0.5, linewidth=1.5, label='KDE of WPoPS'))


    ax2.set_ylim((ylim[0] - x1[1].item()) * histplotscale, (ylim[1] - x1[1].item()) * histplotscale)
    ax2.set_xlim(*xlim)
    ax2.set_ylabel(" ")
    ax3.set_ylim((ylim[0] - x1[1].item()) * histplotscale, (ylim[1] - x1[1].item()) * histplotscale)
    ax3.set_xlim(*xlim)
    ax3.set_ylabel(" ")

else:
    ax.axvline(x1[0].item())
    values = samples[:, 1]
    ax2 = sns.histplot(y=true_dist[:, 1],  bins=binnr, stat='density', alpha=alphaVal, kde=False, color=true_color,
                       linewidth=linewidth, line_kws=dict(color='black', alpha=0.5, linewidth=1.5, label='KDE of WPoPS'))

    ax3 = sns.histplot(y=values, weights=px, bins=binnr*2, stat='density', alpha=alphaVal-0.2, kde=False, color=sample_color,
                       linewidth=linewidth, line_kws=dict(color='black', alpha=0.5, linewidth=1.5, label='KDE of WPoPS'))
    # plt.setp((ax, ax2), ylim=ylim)
    ax2.set_xlim((xlim[0] - x1[0].item()) * histplotscale, (xlim[1] - x1[0].item()) * histplotscale)
    ax2.set_ylim(*ylim)
    ax2.set_xlabel(" ")

    ax3.set_xlim((xlim[0] - x1[0].item()) * histplotscale, (xlim[1] - x1[0].item()) * histplotscale)
    ax3.set_ylim(*ylim)
    ax3.set_xlabel(" ")

# PLOT LATENT REPRESENTATION
ax4 = fig.add_subplot(142, label="4")
z, _ = model.f(model.normalise(data.to(device)))
z = z.detach().cpu()
min_lim = z.min().item()
max_lim = z.max().item()
ax4.scatter(z[:, 0], z[:, 1], c=yc, alpha=alphaVal) # cmap=colorpal,
ax4.set_title(f'Transformed space ({dataset1})', fontsize=titlesize)
margin = 0.1
ax4.set_xlim(min_lim - margin, max_lim + margin)
ax4.set_ylim(min_lim - margin, max_lim + margin)
ax4.grid(color='gray', linestyle='--', linewidth=0.8, alpha=0.5)
ax4.set_xlabel(r"$\tilde{x}_1$", fontsize=16)
ax4.set_ylabel(r"$\tilde{x}_2$", fontsize=16)
ax4.tick_params(axis='both', which="major", labelsize=12)
#endregion

#region Model2 plot
if dataset2 == "moons":
    histplotscale = 3
elif dataset2 == "blobs":
    histplotscale = 0.05
model = torch.load(model_path2)

# CREATE DATA
if dataset2 == "moons":
    data, y = make_moons(10000, noise=0.1, random_state=7)
    true_threshold = 0.15
elif dataset2 == "blobs":
    data, y = make_blobs(n_samples=10000, centers=6, n_features=2, random_state=7)
    true_threshold = 0.5
data = torch.tensor(data)
yc = [list(colors.values())[i] for i in y]

# NORMALISE DATA FOR MODELE ESTIMATION
x2 = model.normalise(x2)
samples, px = estimate_missing_values(x2, model, n=10000)
# INVERSE NORMALISATION TO TRANFORM INTO ORIGINAL DOMAIN
x2 = model.inv_normalise(x2).cpu()
samples = model.inv_normalise(samples).detach().cpu()
px = px.cpu()

# DEFINE MASK THAT TAKES ALL DATA AROUND THE KNOWN DATA-POINT WITH THRESHOLD "true_threshold"
if torch.isnan(x2[0]):
    true_mask = torch.logical_and(data[:, 1] > x2[1] - true_threshold, data[:, 1] < x2[1] + true_threshold)
    true_dist = data[true_mask]
else:
    true_mask = torch.logical_and(data[:, 0] > x2[0] - true_threshold, data[:, 0] < x2[0] + true_threshold)
    true_dist = data[true_mask]

# DEFINE PLOT LIMITS
ylim = (min(data.cpu()[:, 1])-0.2, max(data.cpu()[:, 1])+0.2)
xlim = (min(data.cpu()[:, 0])-0.2, max(data.cpu()[:, 0])+0.2)

## PLOT BLOBS
ax = fig.add_subplot(143, label="1")
ax2 = fig.add_subplot(143, label="5", frame_on=False)
ax3 = fig.add_subplot(143, label="6", frame_on=False)

ax2.set_xticks([])
ax2.set_yticks([])
ax3.set_xticks([])
ax3.set_yticks([])

ax.scatter(data.cpu()[:, 0], data.cpu()[:, 1], c=yc, alpha=alphaVal) # cmap=colorpal
ax.set_title(plot_title2, fontsize=titlesize)
ax.grid(color='gray', linestyle='--', linewidth=0.8, alpha=0.5)
ax.set_xlabel(r"$x_1$", fontsize=16)
ax.set_ylabel(r"$x_2$", fontsize=16)
ax.tick_params(axis='both', which="major", labelsize=12)
ax.set_ylim(*ylim)
ax.set_xlim(*xlim)

if x2[0].isnan():
    ax.axhline(x2[1].item())
    values = samples[:, 0]

    ax2 = sns.histplot(x=true_dist[:, 0], bins=binnr, stat='density', alpha=alphaVal, kde=False, color=true_color,
                       linewidth=linewidth, line_kws=dict(color='goldenrod', alpha=0.5, linewidth=1.5, label='KDE of WPoPS'))

    ax3 = sns.histplot(x=values, weights=px, bins=binnr*2, stat='density', alpha=alphaVal-0.2, kde=False, color=sample_color,
                       linewidth=linewidth, line_kws=dict(color='royalblue', alpha=0.5, linewidth=1.5, label='KDE of WPoPS'))


    ax2.set_ylim((ylim[0] - x2[1].item()) * histplotscale, (ylim[1] - x2[1].item()) * histplotscale)
    ax2.set_xlim(*xlim)
    ax2.set_ylabel(" ")
    ax3.set_ylim((ylim[0] - x2[1].item()) * histplotscale, (ylim[1] - x2[1].item()) * histplotscale)
    ax3.set_xlim(*xlim)
    ax3.set_ylabel(" ")

else:
    ax.axvline(x2[0].item())
    values = samples[:, 1]
    ax2 = sns.histplot(y=true_dist[:, 1],  bins=binnr, stat='density', alpha=alphaVal, kde=False, color=true_color,
                       linewidth=linewidth, line_kws=dict(color='black', alpha=0.5, linewidth=1.5, label='KDE of WPoPS'))

    ax3 = sns.histplot(y=values, weights=px, bins=binnr*2, stat='density', alpha=alphaVal-0.2, kde=False, color=sample_color,
                       linewidth=linewidth, line_kws=dict(color='black', alpha=0.5, linewidth=1.5, label='KDE of WPoPS'))
    # plt.setp((ax, ax2), ylim=ylim)
    ax2.set_xlim((xlim[0] - x2[0].item()) * histplotscale, (xlim[1] - x2[0].item()) * histplotscale)
    ax2.set_ylim(*ylim)
    ax2.set_xlabel(" ")

    ax3.set_xlim((xlim[0] - x2[0].item()) * histplotscale, (xlim[1] - x2[0].item()) * histplotscale)
    ax3.set_ylim(*ylim)
    ax3.set_xlabel(" ")

# PLOT LATENT REPRESENTATION
ax4 = fig.add_subplot(144, label="7")
z, _ = model.f(model.normalise(data.to(device)))
z = z.detach().cpu()
min_lim = z.min().item()
max_lim = z.max().item()
ax4.scatter(z[:, 0], z[:, 1], c=yc, alpha=alphaVal) # cmap=colorpal,
ax4.set_title(f'Transformed space ({dataset2})', fontsize=titlesize)
margin = 0.1
ax4.set_xlim(min_lim - margin, max_lim + margin)
ax4.set_ylim(min_lim - margin, max_lim + margin)
ax4.grid(color='gray', linestyle='--', linewidth=0.8, alpha=0.5)
ax4.set_xlabel(r"$\tilde{x}_1$", fontsize=16)
ax4.set_ylabel(r"$\tilde{x}_2$", fontsize=16)
ax4.tick_params(axis='both', which="major", labelsize=12)
#endregion

#region LEGEND
colour_list = [true_color, sample_color]
true_dist = colour_list[0]
sample_dist = colour_list[1]
labels = ["True Distribution", "Sample Distribution"]
f = lambda m,c: plt.plot([],[],marker=m, color=c, ls="none")[0]
true_patch = mpatches.Patch(color=true_dist, label='Action', alpha=alphaVal)
sample_patch = mpatches.Patch(color=sample_dist, label='Adventure', alpha=alphaVal-0.2)
handles=[true_patch, sample_patch]
# fig.subplots_adjust(bottom=0.05, top=0.1)
legend = fig.legend(handles, labels, loc='lower center', bbox_to_anchor=(0.5, -0.02), ncol=2, framealpha=1, frameon=True, prop={'size': 20})
#endregion

plt.tight_layout(rect=[0,0.075,1,1])

plt.savefig(f"moons_n_blobs_n_shit_p{drp_rate1}.pdf")
plt.show()



