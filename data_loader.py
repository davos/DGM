import pandas as pd
import torch
from torch import nn
import sys
from sklearn.datasets import make_moons
from sklearn.model_selection import train_test_split
import Helpers


class DataLoader2(nn.Module):
    """Data loader module for training MCFlow
    Args:
        mode (int): Determines if we are loading training or testing data
        seed (int): Used to determine the fold for cross validation experiments or reproducibility consideration if not
        path (str): Determines the dataset to load
        drp_percent (float): Determines the binomial success rate for observing a feature
    """
    MODE_TRAIN = 0
    MODE_TEST = 1

    def __init__(self, data, drp_percent=0.2, dataset="moons", device="cpu", normalising_strategy='mustd'):
        self.dataset = dataset
        self.device = device
        x, y = data
        self.attribute_type = self.attribute_list()
        self.dequantization_type = self.dequantization_list()
        self.train_x, self.test_x, self.train_y, self.test_y = train_test_split(x, y, test_size=0.1, random_state=214)

        if dataset == "credit":
            self.train_x = Helpers.remove_data(self.train_x.clone(), p=drp_percent)
        else:
            self.train_x = Helpers.remove_data2(self.train_x.clone(), p=drp_percent)

        self.normalising_strategy = normalising_strategy

    def attribute_list(self):
        # Returns boolean list with true if attribute is continuous
        if self.dataset == "moons" or self.dataset == "circles" or self.dataset == "blobs":
            return torch.tensor([True, True])
        elif self.dataset == "credit":
            return torch.tensor([True, False, False, False, True, False, False, False, False, False, False, True, True,
                                 True, True, True, True, True, True, True, True, True, True])

    def dequantization_list(self):
        # Returns boolean list with true if attribute is continuous
        if self.dataset == "moons" or self.dataset == "circles" or self.dataset == "blobs":
            return torch.tensor([False, False])
        elif self.dataset == "credit":
            return torch.tensor([False, True, True, True, True, True, True, True, True, True, True, False, False,
                                 False, False, False, False, False, False, False, False, False, False])

    def create_k_fold(self, seed):
        # I will create different folds based on the seed
        fold_sz = int(self.train_x.shape[0] * .2)  # 5 folds

        if seed == 0:
            train_x = self.train_x[fold_sz:]
            train_y = self.train_y[fold_sz:]
            val_x = self.train_x[:fold_sz]
            val_y = self.train_y[:fold_sz]
        elif seed == 1:
            train_x = torch.cat((self.train_x[:fold_sz], self.train_x[int(fold_sz * 2):]))
            train_y = torch.cat((self.train_y[:fold_sz], self.train_y[int(fold_sz * 2):]))
            val_x = self.train_x[fold_sz:int(fold_sz * 2)]
            val_y = self.train_y[fold_sz:int(fold_sz * 2)]
        elif seed == 2:
            train_x = torch.cat((self.train_x[:int(fold_sz * 2)], self.train_x[int(fold_sz * 3):]))
            train_y = torch.cat((self.train_y[:int(fold_sz * 2)], self.train_y[int(fold_sz * 3):]))
            val_x = self.train_x[int(fold_sz * 2):int(fold_sz * 3)]
            val_y = self.train_y[int(fold_sz * 2):int(fold_sz * 3)]
        elif seed == 3:
            train_x = torch.cat((self.train_x[:int(fold_sz * 3)], self.train_x[int(fold_sz * 4):]))
            train_y = torch.cat((self.train_y[:int(fold_sz * 3)], self.train_y[int(fold_sz * 4):]))
            val_x = self.train_x[int(fold_sz * 3):int(fold_sz * 4)]
            val_y = self.train_y[int(fold_sz * 3):int(fold_sz * 4)]
        elif seed == 4:
            train_x = self.train_x[:int(fold_sz * 4)]
            train_y = self.train_y[:int(fold_sz * 4)]
            val_x = self.train_x[int(4 * fold_sz):]
            val_y = self.train_y[int(4 * fold_sz):]
        else:
            print("incorrect seed for the fold")
            sys.exit()

        # if self.contrun:
        #     train_x, val_x, train_min, train_max = self.normalize(train_x[:, self.attribute_type], val_x[:, self.attribute_type])
        #     self.dequantization_type = self.dequantization_type[self.attribute_type]
        # else:

        if self.normalising_strategy == 'minmax':
            train_x, val_x, train_min, train_max = self.normalise(train_x, val_x)
            # train_mu, train_sig = self.calculate_mu_sig(train_x)
            return train_x, train_min, train_max, train_y, val_x, val_y  # train_mu, train_sig,
        else:
            train_x, val_x, train_mu, train_sig = self.standardise(train_x, val_x)
            return train_x, train_mu, train_sig, train_y, val_x, val_y

    def standardise(self, x_train, x_val):
        mu = torch.tensor([torch.mean(x_train[:, i][~torch.isnan(x_train[:, i])]) for i in range(x_train.shape[1])]).to(self.device)
        sig = torch.tensor([torch.std(x_train[:, i][~torch.isnan(x_train[:, i])]) for i in range(x_train.shape[1])]).to(self.device)
        x_train = (x_train - mu)/sig
        x_val = (x_val - mu) / sig
        return x_train, x_val, mu, sig

    def normalise(self, x_train, x_val):
        x_min = torch.tensor([torch.min(x_train[:, i][~torch.isnan(x_train[:, i])]) for i in range(x_train.shape[1])]).to(self.device)
        x_max = torch.tensor([torch.max(x_train[:, i][~torch.isnan(x_train[:, i])]) for i in range(x_train.shape[1])]).to(self.device)
        x_train = (x_train - x_min)/(x_max-x_min)
        x_val = (x_val - x_min)/(x_max-x_min)
        return x_train, x_val, x_min, x_max

    def inv_normalize(self, x_train, x_val, min, max):
        x_train = x_train * (max-min) + min
        x_val = x_val * (max-min) + min
        return x_train, x_val

