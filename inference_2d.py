import os.path

from Helpers import *
from data_loader import DataLoader2
import torch.nn as nn
from glob import glob
import json

device = torch.device('cuda') if torch.cuda.is_available() else torch.device('cpu')

data_type = "complete_data/moons/"
methods = ['mode', 'mean']
mse = nn.MSELoss()
batch_size = 16

p_folders = glob(data_type + "p0*")

for p_folder in p_folders:
    print(f'Working on: {p_folder}')

    results = {}
    model_paths = glob(p_folder + '/*.model')
    if not model_paths or os.path.exists(p_folder + '/results.json'):
        print(f'results.json found or no model found in: {p_folder}')
        continue
    models = [torch.load(path) for path in model_paths]
    test_data, _ = pd.read_pickle(p_folder + '/test_data.pickle')
    test_loader = DataLoader(test_data, batch_size=batch_size, shuffle=False)
    results[p_folder[-4:]] = {}
    for method in methods:
        predicted_values, target_values = torch.Tensor([]).to(device), torch.Tensor([]).to(device)
        for model in models:
            test_data_norm = model.normalise(test_data.clone())
            test_loader_norm = DataLoader(test_data_norm, batch_size=batch_size, shuffle=False)
            iterator = tqdm(zip(test_loader, test_loader_norm), total=len(test_data)/16)
            for batch, batch_norm in iterator:
                for col in range(2):
                    nan_batch = batch_norm.clone()
                    nan_batch[:, col] = np.nan
                    nan_mask = torch.isnan(nan_batch)
                    out = inference(nan_batch.clone().detach(), model, n=1000, method=method)
                    out = model.inv_normalise(out)
                    predicted_values = torch.cat((predicted_values, out[nan_mask].to(device)), 0)
                    target_values = torch.cat((target_values, batch[nan_mask]), 0)

        RMSE = torch.sqrt(mse(predicted_values, target_values)).item()
        results[p_folder[-4:]][method] = RMSE
        with open(p_folder + '/results.json', 'w') as fp:
            json.dump(results, fp)
        print(f'\nMethod: {method}, RMSE: {RMSE:.3f}')
